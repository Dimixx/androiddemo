package com.example.dimixx.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Dimixx on 5.7.2017 г..
 */

public class Adapter extends SelectableAdapter<Adapter.MyViewHolder> {
    private LayoutInflater inflater;
    private List<PictureItem> data = Collections.emptyList();
    private Picasso picasso;


    public Adapter(Context context, List<PictureItem> data) {
        inflater = LayoutInflater.from(context);
        picasso = Picasso.with(inflater.getContext());
        this.data = data;
    }

    public void setData(List<PictureItem> data){
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = R.layout.row;
        if (viewType == 1) {
            layout = R.layout.big_row;
        }
        View view = inflater.inflate(layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position).isBig()) {
            return 1;
        }
        return 0;
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PictureItem current = data.get(position);

        picasso.load(current.getPictureUri())
               .fit()
               .centerCrop()
               .into(holder.getImageView());

        //Highlight the item if it's selected
        if (isSelected(position)) {
            holder.itemView.setBackgroundColor(Color.GREEN);
        } else
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
    }

    /**
     * Returns the number of items
     */
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private ImageView image;

        public MyViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.rowPic);
            image.setOnClickListener(this);
            image.setOnLongClickListener(this);
        }

        public ImageView getImageView() {
            return image;
        }

        /**
         * Handles the onClick event
         */
        @Override
        public void onClick(View v) {
            if (!isInSelectionMode()) {
                Toast.makeText(inflater.getContext(), "Preview is not available", Toast.LENGTH_SHORT).show();
            } else {
                toggleSelection(getLayoutPosition());
            }
        }

        /**
         * Handles the onLongClick event
         */
        @Override
        public boolean onLongClick(View v) {
            if (!isInSelectionMode()) {
                setInSelectionMode(true);
                toggleSelection(getLayoutPosition());
                return true;
            }
            return false;
        }
    }

    /**
     * Deletes one item
     * @param position Position of the item to delete
     */
    public void deleteItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        clearSelection();
    }

    /**
     * Deletes multiple item
     * @param positions List of the positions to delete
     */
    public void deleteItems(List<Integer> positions) {
        // Reverse-sort the list
        Collections.sort(positions, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return rhs - lhs;
            }
        });

        // Split the list in ranges
        while (!positions.isEmpty()) {
            if (positions.size() == 1) {
                deleteItem(positions.get(0));
                positions.remove(0);
            } else {
                int count = 1;
                // Getting the ranges
                while (positions.size() > count && positions.get(count).equals(positions.get(count - 1) - 1)) {
                    ++count;
                }

                if (count == 1) {
                    deleteItem(positions.get(0));
                } else {
                    deleteRange(positions.get(count - 1), count);
                }

                for (int i = 0; i < count; ++i) {
                    positions.remove(0);
                }
            }
        }
    }

    /**
     * Deletes range of items
     * @param positionStart Starting position
     * @param itemCount Number of items
     */
    private void deleteRange(int positionStart, int itemCount) {
        for (int i = 0; i < itemCount; ++i) {
            data.remove(positionStart);
        }
        notifyItemRangeRemoved(positionStart, itemCount);
    }
}
