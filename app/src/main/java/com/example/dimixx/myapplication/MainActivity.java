package com.example.dimixx.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.content.pm.PackageManager.PERMISSION_DENIED;

public class MainActivity extends AppCompatActivity {
    final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1000;
    private boolean canReadExternalStorage;
    private List<PictureItem> invisibleData;
    private List<PictureItem> visibleData;
    Menu mMenu;

    private RecyclerView recyclerView;
    private Adapter adapter;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        invisibleData = new ArrayList<>();
        visibleData = new ArrayList<>();

        //initializing add button
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new OnClickListener() {
            /**
             * Handling OnClick event of the add button
             */
            @Override
            public void onClick(View view) {
                if (!invisibleData.isEmpty()) {
                    visibleData.add(0, invisibleData.get(0));
                    invisibleData.remove(0);
                    adapter.notifyItemInserted(0);
                    recyclerView.smoothScrollToPosition(0);
                } else {
                    Toast.makeText(getApplicationContext(), "Nothing to add!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //checking permission for reading external storage
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionCheck == PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            canReadExternalStorage = true;
        }

        //initializing the adapter
        adapter = new Adapter(this, this.visibleData);
        adapter.setOnSelectionListener(selectionListener);

        //initializing the recyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        int pictureSize = getResources().getDimensionPixelSize(R.dimen.pic_size);

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int columns = displayMetrics.widthPixels / pictureSize;
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(columns, StaggeredGridLayoutManager.VERTICAL));

        //checking when the activity is created if savedInstanceState is null then we are created for the first time
        if (savedInstanceState == null){
            //get all Pictures
            getData();
        }
        else{
            visibleData = savedInstanceState.getParcelableArrayList("VisibleItems");
            adapter.setData(visibleData);
            invisibleData = savedInstanceState.getParcelableArrayList("InvisibleItems");
            List<Integer> selectedItems = savedInstanceState.getIntegerArrayList("SelectedItems");
            for (int i = 0; i < selectedItems.size(); i++){
                adapter.toggleSelection(selectedItems.get(i));
            }
            adapter.setInSelectionMode(savedInstanceState.getBoolean("SelectionMode"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mMenu = menu;
        updateSelectionMenuItem(adapter.isInSelectionMode());
        return true;
    }

    /**
     * Handling menu button clicks
     * @param item Clicked menu item
     * @return Return true if the click is handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_select) {
            adapter.setInSelectionMode(!adapter.isInSelectionMode());
            return true;
        }

        if (id == R.id.action_delete) {
            adapter.deleteItems(adapter.getSelectedItems());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Reading the request permission dialog's input
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    canReadExternalStorage = true;
                    //get all Pictures
                    getData();
                } else {
                    canReadExternalStorage = false;
                }
            }
        }
    }

    /**
     * Used to save state information
     * @param outState Collection of key-value pairs
     */
    @Override
    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList("VisibleItems", new ArrayList<PictureItem>(visibleData));
        outState.putParcelableArrayList("InvisibleItems", new ArrayList<PictureItem>(invisibleData));
        outState.putIntegerArrayList("SelectedItems", new ArrayList<Integer>(adapter.getSelectedItems()));
        outState.putBoolean("SelectionMode", adapter.isInSelectionMode());
    }

    /**
     * Getting the pictures from the external storage
     */
    private void getData() {
        if (canReadExternalStorage) {
            File picsDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DemoPictures");
            if (picsDirectory.exists()) {
                for (int j = 0; j < picsDirectory.listFiles().length; j++) {
                    Uri file = Uri.fromFile(picsDirectory.listFiles()[j]);
                    boolean showBig = (j+1)%6==0;
                    if (j < picsDirectory.listFiles().length / 2)
                        visibleData.add(new PictureItem(file, showBig));
                    else
                        invisibleData.add(new PictureItem(file, showBig));
                }
                adapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * Setting application in/out of selection mode
     */
    private OnSelectionListener selectionListener = new OnSelectionListener() {
        @Override
        public void onSelectionStateChanged(boolean inSelection) {
            if (!inSelection){
                Toast.makeText(getApplicationContext(), "Selection Mode is Off", Toast.LENGTH_SHORT).show();
                fab.setVisibility(View.VISIBLE);
            }
            else{
                Toast.makeText(getApplicationContext(), "Selection Mode is On", Toast.LENGTH_SHORT).show();
                fab.setVisibility(View.GONE);
            }

            updateSelectionMenuItem(inSelection);
        }
    };

    private void updateSelectionMenuItem(boolean inSelection){
        if (mMenu != null) {
            MenuItem item = mMenu.findItem(R.id.action_select);
            if (inSelection) {
                item.setIcon(R.drawable.ic_selected);
            } else {
                item.setIcon(R.drawable.ic_select);
            }
        }
    }

}
