package com.example.dimixx.myapplication;

/**
 * Created by Dimixx on 12.7.2017 г..
 */

/**
 * Hook listener for selection change
 */
public interface OnSelectionListener {
    void onSelectionStateChanged(boolean inSelection);
}
