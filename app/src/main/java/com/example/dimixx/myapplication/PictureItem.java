package com.example.dimixx.myapplication;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dimixx on 5.7.2017 г..
 */

public class PictureItem implements Parcelable{
    private Uri pictureUri;
    private boolean isBig;

    protected PictureItem(Parcel in) {
        pictureUri = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Creator<PictureItem> CREATOR = new Creator<PictureItem>() {
        @Override
        public PictureItem createFromParcel(Parcel in) {
            return new PictureItem(in);
        }

        @Override
        public PictureItem[] newArray(int size) {
            return new PictureItem[size];
        }
    };

    public void setPictureUri(Uri pictureUri) {
        this.pictureUri = pictureUri;
    }

    public Uri getPictureUri() {
        return pictureUri;
    }
    public PictureItem(Uri startPictureUri, boolean isBig){
        setPictureUri(startPictureUri);
        setBig(isBig);
    }

    public boolean isBig() {
        return isBig;
    }

    public void setBig(boolean big) {
        isBig = big;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(pictureUri, flags);
    }
}
